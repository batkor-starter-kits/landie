### INSTALL

1. `git clone https://gitlab.com/batkor-starter-kits/landie.git`
2. `composer install`
3. `drush ci --existing-config`
