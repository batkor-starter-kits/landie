/**
 * @file
 * This Gulp config file.
 */

'use strict'

const gulp = require("gulp");
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const sassGlob = require('gulp-sass-glob');
const plumber = require('gulp-plumber');
const paths = {
  styles: {
    // Source files.
    src: "assets/scss/**/*.scss",
    // Compiled files output.
    dest: "assets/css",
  }
}

function scss() {
  return gulp
    .src(paths.styles.src)
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(sassGlob())
    .pipe(sass())
    .pipe(autoprefixer({
      overrideBrowserslist: ['last 2 versions'],
      cascade: false
    }))
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(paths.styles.dest));
}

function watchFiles() {
  scss();
  gulp.watch(paths.styles.src, gulp.series('scss'));

}


exports.scss = scss;
exports.watch = exports.default = watchFiles;
