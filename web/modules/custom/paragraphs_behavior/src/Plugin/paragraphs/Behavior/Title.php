<?php

namespace Drupal\paragraphs_behavior\Plugin\paragraphs\Behavior;

use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;

/**
 * @ParagraphsBehavior(
 *   id = "paragraph_behavior_title",
 *   label = @Translation("Paragraph title element"),
 *   description = @Translation("Allows to select HTML wrapper for title."),
 *   weight = 0,
 * )
 */
class Title extends BaseBehavior {

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    if ($paragraph->hasField('field_title')) {
      $form['title_element'] = [
        '#type' => 'details',
        '#title' => $this->t('Title element'),
      ];

      $form['title_element']['tag'] = [
        '#type' => 'select',
        '#title' => $this->t('Tag'),
        '#description' => $this->t('Select tag for title element.'),
        '#options' => $this->getTags(),
        '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), ['title_element', 'tag'], 'h2'),
      ];
      $form['title_element']['attributes'] = $this
        ->attributeFormElement($paragraph->getBehaviorSetting($this->getPluginId(), ['title_element', 'attributes'], ''));
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(Paragraph $paragraph) {
    $summary = [];
    $title_element = $paragraph->getBehaviorSetting($this->getPluginId(), 'title_element');
    if ($title_element) {
      $summary[] = $this->t('Title element: @tag; @attributes', [
        '@tag' => $title_element['tag'] ?? '',
        '@attributes' => $title_element['attributes'] ?? '',
      ]);
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function preprocess(&$variables) {
    $variables['content']['field_title']['#attributes'] = $this
      ->parseAttribute(
        $variables['elements']['#paragraph']->getBehaviorSetting($this->getPluginId(), ['title_element', 'attributes'], '')
      );
  }

  /**
   * Return options for heading elements.
   */
  protected function getTags() {
    return [
      'h1' => '<h1>',
      'h2' => '<h2>',
      'h3' => '<h3>',
      'h4' => '<h4>',
      'h5' => '<h5>',
      'h6' => '<h6>',
      'div' => '<div>',
    ];
  }

}
