<?php

namespace Drupal\paragraphs_behavior\Plugin\paragraphs\Behavior;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * Provides base behavior.
 */
abstract class BaseBehavior extends ParagraphsBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {}

  /**
   * Returns attribute form element.
   *
   * @param mixed $default_value
   *   The default value for element.
   *
   * @return array
   *   The attribute form element.
   */
  protected function attributeFormElement($default_value) {
    return [
      '#type' => 'textarea',
      '#title' => $this->t('Attributes'),
      '#description' => $this->t('Element attributes. Example:<br><code>class|my_class1 my_class2<br>attribute_name|attribute_value</code>'),
      '#default_value' => $default_value,
      '#rows' => 3,
    ];
  }

  /**
   * Returns attribute array after parse.
   *
   * @param string $source
   *   The source attribute values.
   *
   * @return array
   *   The attribute array.
   */
  protected function parseAttribute($source) {
    $attributes = [];
    $source = str_replace(["\r\n", "\r"], "\n", $source);
    foreach (explode("\n", $source) as $item) {
      $item = trim($item);
      // Skip if empty.
      if (empty($item)) {
        continue;
      }
      $parse = explode('|', $item);
      // Skip if "|" not found.
      if (count($parse) < 2) {
        continue;
      }

      $attributes[$parse[0]] = $parse[1];
    }
    return $attributes;
  }

}