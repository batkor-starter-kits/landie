<?php

namespace Drupal\paragraphs_behavior\Plugin\paragraphs\Behavior;

use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;

/**
 * @ParagraphsBehavior(
 *   id = "paragraph_behavior_description",
 *   label = @Translation("Paragraph description element"),
 *   description = @Translation("Allows to select HTML wrapper for title."),
 *   weight = 0,
 * )
 */
class Description extends BaseBehavior {

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    if ($paragraph->hasField('field_description')) {
      $form['description_element'] = [
        '#type' => 'details',
        '#title' => $this->t('Description element'),
      ];

      $form['description_element']['tag'] = [
        '#type' => 'select',
        '#title' => $this->t('Tag'),
        '#description' => $this->t('Select tag for description element.'),
        '#options' => $this->getTags(),
        '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), ['description_element', 'tag'], 'h2'),
      ];
      $form['description_element']['attributes'] = $this
        ->attributeFormElement($paragraph->getBehaviorSetting($this->getPluginId(), ['description_element', 'attributes'], ''));
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(Paragraph $paragraph) {
    $summary = [];
    $title_element = $paragraph->getBehaviorSetting($this->getPluginId(), 'description_element');
    if ($title_element) {
      $summary[] = $this->t('Description element: @tag, @attributes', [
        '@tag' => $title_element['tag'] ?? '',
        '@attributes' => $title_element['attributes'] ?? '',
      ]);
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function preprocess(&$variables) {
    $variables['content']['field_description']['#attributes'] = $this
      ->parseAttribute(
        $variables['elements']['#paragraph']->getBehaviorSetting($this->getPluginId(), ['description_element', 'attributes'], '')
      );
  }

  /**
   * Return options for heading elements.
   */
  protected function getTags() {
    return [
      'p' => '<p>',
      'div' => '<div>',
    ];
  }

}